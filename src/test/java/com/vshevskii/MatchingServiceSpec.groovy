package com.vshevskii

import com.vshevskii.dto.*
import com.vshevskii.enums.Country
import com.vshevskii.messages.ErrorMessages
import com.vshevskii.service.MatchingServiceImpl
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime

@SpringBootTest
class MatchingServiceSpec extends Specification {

    def matchingService = new MatchingServiceImpl()

    @Shared
    def now = LocalDateTime.now()

    @Shared
    def playerId = "mockPlayerId"

    @Shared
    def campaignId = 1234

    def 'checkLevelTest'() {

        expect:
        matchingService.checkLevel(LevelDto.builder()
                .min(min)
                .max(max)
                .build(), userLevel) == expected

        where:
        min | max | userLevel | expected
        1   | 10  | 0         | false
        1   | 10  | 1         | true
        1   | 10  | 5         | true
        1   | 10  | 10        | true
        1   | 10  | 11        | false
    }

    def 'checkLevelMinMoreThanMaxTest'() {

        given:
        def level = LevelDto.builder()
                .min(10)
                .max(9)
                .build()

        when:
        matchingService.checkLevel(level, 0)

        then:
        thrown(IllegalArgumentException)
    }

    def 'checkCountriesTest'() {

        expect:
        matchingService.checkCountries(requiredCountries, userCountry) == expected

        where:
        requiredCountries                                 | userCountry | expected
        Arrays.asList(Country.CA, Country.RO, Country.US) | Country.CA  | true
        Arrays.asList(Country.CA, Country.RO)             | Country.CA  | true
        Arrays.asList(Country.CA, Country.RO)             | Country.US  | false
        Arrays.asList()                                   | Country.US  | false
    }

    def 'checkRequiredItemsTest'() {

        expect:
        matchingService.checkRequiredItems(requiredItems, usedItems) == expected

        where:
        requiredItems                     | usedItems                         | expected
        Arrays.asList("item_1", "item_2") | Arrays.asList("item_1", "item_2") | true
        Arrays.asList("item_1", "item_2") | Arrays.asList("item_1", "item_3") | false
        Arrays.asList("item_1", "item_2") | Arrays.asList("item_3", "item_4") | false
        Arrays.asList("item_1", "item_2") | Arrays.asList()                   | false
        Arrays.asList()                   | Arrays.asList()                   | true
        Arrays.asList()                   | Arrays.asList("item_1", "item_2") | true
    }

    def 'checkForbiddenItemsTest'() {

        expect:
        matchingService.checkForbiddenItems(forbiddenItems, usedItems) == expected

        where:
        forbiddenItems                    | usedItems                         | expected
        Arrays.asList("item_1", "item_2") | Arrays.asList("item_1", "item_2") | false
        Arrays.asList("item_1", "item_2") | Arrays.asList("item_1", "item_3") | false
        Arrays.asList("item_1", "item_2") | Arrays.asList("item_3", "item_4") | true
        Arrays.asList("item_1", "item_2") | Arrays.asList()                   | true
        Arrays.asList()                   | Arrays.asList()                   | true
        Arrays.asList()                   | Arrays.asList("item_1", "item_2") | true
    }

    def 'checkStartEndDatesTest'() {

        expect:
        matchingService.checkStartEndDates(campaignStartDateTime, campaignEndDateTime, currentDateTime) == expected

        where:
        campaignStartDateTime                      | campaignEndDateTime                        | currentDateTime                            | expected
        LocalDateTime.parse("2022-01-25T00:00:00") | LocalDateTime.parse("2022-02-25T00:00:00") | LocalDateTime.parse("2022-03-25T00:00:00") | false
        LocalDateTime.parse("2022-01-25T00:00:00") | LocalDateTime.parse("2022-02-25T00:00:00") | LocalDateTime.parse("2021-01-25T00:00:00") | false
        LocalDateTime.parse("2022-01-25T00:00:00") | LocalDateTime.parse("2022-02-25T00:00:00") | LocalDateTime.parse("2022-01-25T00:00:00") | true
        LocalDateTime.parse("2022-01-25T00:00:00") | LocalDateTime.parse("2022-02-25T00:00:00") | LocalDateTime.parse("2022-02-25T00:00:00") | true
        LocalDateTime.parse("2022-01-25T00:00:00") | LocalDateTime.parse("2022-02-25T00:00:00") | LocalDateTime.parse("2022-01-26T00:00:00") | true
    }

    def 'matchCampaignForUserTest'() {

        given:
        def campaign = CampaignDto.builder()
                .id(campaignId)
                .matchers(MatcherDto.builder()
                        .level(LevelDto.builder()
                                .min(min)
                                .max(max)
                                .build())
                        .has(RequirementDto.builder()
                                .countries(countries)
                                .items(requiredItems)
                                .build())
                        .doesNotHave(ExceptRequirementDto.builder()
                                .items(forbiddenItems)
                                .build()).build())
                .startDate(startDateTime)
                .endDate(endDateTime)
                .enabled(enabled)
                .build()
        def userProfile = UserProfileDto.builder()
                .playerId(playerId)
                .level(userLevel)
                .country(country)
                .inventory(InventoryDto.builder()
                        .items(items)
                        .build())
                .build()

        expect:
        def actual = matchingService.matchCampaignForUser(campaign, userProfile)
        actual.getErrorMessage() == message
        actual.isSuccess() == success

        where:
        min | max | countries                 | requiredItems           | forbiddenItems          | startDateTime      | endDateTime       | enabled | userLevel | country           | items                   | message                                                                                                                        | success
        10  | 20  | Arrays.asList(Country.US) | Arrays.asList("item_1") | Arrays.asList("item_2") | now.minusMonths(1) | now.minusDays(1)  | false   | 9         | Country.CA.name() | Arrays.asList("item_2") | String.format(ErrorMessages.INVALID_USER_LEVEL, playerId, 9, 10, 20)                                                           | false
        9   | 20  | Arrays.asList(Country.US) | Arrays.asList("item_1") | Arrays.asList("item_2") | now.minusMonths(1) | now.minusDays(1)  | false   | 9         | Country.CA.name() | Arrays.asList("item_2") | String.format(ErrorMessages.INVALID_USER_COUNTRY, playerId, Country.CA.name(), Arrays.asList(Country.US))                      | false
        9   | 20  | Arrays.asList(Country.CA) | Arrays.asList("item_1") | Arrays.asList("item_2") | now.minusMonths(1) | now.minusDays(1)  | false   | 9         | Country.CA.name() | Arrays.asList("item_2") | String.format(ErrorMessages.REQUIRED_ITEM_IS_MISSING, playerId, Arrays.asList("item_2"), Arrays.asList("item_1"))              | false
        9   | 20  | Arrays.asList(Country.CA) | Arrays.asList()         | Arrays.asList("item_2") | now.minusMonths(1) | now.minusDays(1)  | false   | 9         | Country.CA.name() | Arrays.asList("item_2") | String.format(ErrorMessages.FORBIDDEN_ITEM_IS_USED, playerId, Arrays.asList("item_2"), Arrays.asList("item_2"))                | false
        9   | 20  | Arrays.asList(Country.CA) | Arrays.asList()         | Arrays.asList()         | now.minusMonths(1) | now.minusDays(1)  | false   | 9         | Country.CA.name() | Arrays.asList("item_2") | String.format(ErrorMessages.INVALID_DATE, now.toLocalDate(), now.minusMonths(1).toLocalDate(), now.minusDays(1).toLocalDate()) | false
        9   | 20  | Arrays.asList(Country.CA) | Arrays.asList()         | Arrays.asList()         | now.minusMonths(1) | now.plusMonths(1) | false   | 9         | Country.CA.name() | Arrays.asList("item_2") | String.format(ErrorMessages.DISABLED, campaignId, false)                                                                       | false
        9   | 20  | Arrays.asList(Country.CA) | Arrays.asList()         | Arrays.asList()         | now.minusMonths(1) | now.plusMonths(1) | true    | 9         | Country.CA.name() | Arrays.asList("item_2") | null                                                                                                                           | true
    }
}
