package com.vshevskii.controllers;

import com.vshevskii.dto.CampaignDto;
import com.vshevskii.dto.UserProfileDto;
import com.vshevskii.service.CurrentCampaignService;
import com.vshevskii.service.UserProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProfileMatchController {

    private final UserProfileService userProfileService;

    private final CurrentCampaignService currentCampaignService;

    @GetMapping("/get_client_config/{player_id}")
    public UserProfileDto getClientConfig(@PathVariable("player_id") String playerId) {
        return userProfileService.findById(playerId);
    }

    @GetMapping("/get_client_config")
    public List<UserProfileDto> getClientsConfig() {
        return userProfileService.findAll();
    }

    @PostMapping("/activate_available_campaigns/{player_id}")
    public void activateCampaignsForUserByPlayerId(@PathVariable("player_id") String playerId) {
        List<CampaignDto> currentCampaigns = currentCampaignService.getCurrentActiveCampaigns();
        UserProfileDto foundUserProfile = userProfileService.findById(playerId);
        currentCampaignService.activateCampaignForUser(currentCampaigns, foundUserProfile);
    }

    @PostMapping("/activate_available_campaigns")
    public void activateCampaignsForAllPlayers() {
        List<CampaignDto> currentCampaigns = currentCampaignService.getCurrentActiveCampaigns();
        List<UserProfileDto> foundUserProfiles = userProfileService.findAll();
        for (UserProfileDto userProfile : foundUserProfiles) {
            currentCampaignService.activateCampaignForUser(currentCampaigns, userProfile);
        }
    }
}
