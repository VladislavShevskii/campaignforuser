package com.vshevskii.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

import static org.hibernate.annotations.CascadeType.ALL;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {

    @Id
    @GeneratedValue
    private int id;

    private BigDecimal cash;

    private int coins;

    @ElementCollection
    private List<String> items;

    @OneToOne(mappedBy = "inventory", fetch = FetchType.EAGER)
    @Cascade(value = ALL)
    private UserProfile userProfile;
}
