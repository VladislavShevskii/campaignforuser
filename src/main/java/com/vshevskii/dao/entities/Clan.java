package com.vshevskii.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Clan {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    @OneToOne(mappedBy = "inventory", fetch = FetchType.EAGER)
    private UserProfile userProfile;
}
