package com.vshevskii.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vshevskii.enums.Country;
import com.vshevskii.enums.Gender;
import com.vshevskii.enums.Language;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile {

    @Id
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID playerId;

    private String credential;

    private LocalDateTime created;

    private LocalDateTime modified;

    private LocalDateTime lastSession;

    private BigDecimal totalSpent;

    private BigDecimal totalRefund;

    private int totalTransactions;

    private LocalDateTime lastPurchase;

    @ElementCollection
    private List<String> activeCampaigns;

    @JsonIgnore
    @OneToMany(mappedBy = "userProfile", cascade = javax.persistence.CascadeType.ALL)
    private List<Device> devices;

    private int level;

    private int xp;

    private int totalPlaytime;

    @Enumerated
    private Country country;

    @Enumerated
    private Language language;

    private LocalDate birthdate;

    @Enumerated
    private Gender gender;

    @OneToOne
    @Cascade(CascadeType.ALL)
    private Inventory inventory;

    private String customField;

    @OneToOne
    private Clan clan;
}
