package com.vshevskii.dao;

import com.vshevskii.dao.entities.Clan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClanRepository extends JpaRepository<Clan, Integer> {
}
