package com.vshevskii.messages;

public class ErrorMessages {

    public static String INVALID_USER_LEVEL = "User with id '%s' has level $d, but required min level = %d, and " +
            "required max level = %d";

    public static String INVALID_USER_COUNTRY = "User with id '%s' has country %s, but required countries (%s)";

    public static String REQUIRED_ITEM_IS_MISSING = "User with id '%s' has items (%s), but required items (%s)";

    public static String FORBIDDEN_ITEM_IS_USED = "User with id '%s' has forbidden item, user items (%s), " +
            "forbidden items (%s)";

    public static String INVALID_DATE = "Current date (%s) is outside of the start date (%s) and end date (%s)";

    public static String DISABLED = "Campaign with id '%s' is disabled, 'enabled' field is %b";

    public static String INVALID_LEVEL_MIN_MAX = "Level min cannot be more than level max";
}
