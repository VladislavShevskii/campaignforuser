package com.vshevskii.service;

import com.vshevskii.dto.*;
import com.vshevskii.enums.Country;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MockCurrentCampaignService implements CurrentCampaignService {

    private final MatchingService matchingService;

    private final UserProfileService userProfileService;

    @Override
    public List<CampaignDto> getCurrentActiveCampaigns() {
        return Arrays.asList(
                CampaignDto.builder()
                        .game("mygame")
                        .name("mycampaign")
                        .priority(new BigDecimal("10.5"))
                        .matchers(MatcherDto.builder()
                                .level(LevelDto.builder()
                                        .min(1)
                                        .max(3)
                                        .build())
                                .has(RequirementDto.builder()
                                        .countries(Arrays.asList(Country.US, Country.RO, Country.CA))
                                        .items(Collections.singletonList("item_1"))
                                        .build())
                                .doesNotHave(ExceptRequirementDto.builder()
                                        .items(Collections.singletonList("item_4"))
                                        .build())
                                .build())
                        .startDate(LocalDateTime.parse("2022-01-25T00:00:00"))
                        .endDate(LocalDateTime.parse("2022-02-25T00:00:00"))
                        .enabled(true)
                        .lastUpdated(LocalDateTime.parse("2021-07-13T11:46:58"))
                        .build(),
                CampaignDto.builder()
                        .game("additionalGame")
                        .name("additionalCampaign")
                        .priority(new BigDecimal("20.5"))
                        .matchers(MatcherDto.builder()
                                .level(LevelDto.builder()
                                        .min(1)
                                        .max(10)
                                        .build())
                                .has(RequirementDto.builder()
                                        .countries(Arrays.asList(Country.US, Country.RO, Country.CA))
                                        .items(Collections.singletonList("item_1"))
                                        .build())
                                .doesNotHave(ExceptRequirementDto.builder()
                                        .items(Arrays.asList("item_2", "item_3", "item_4"))
                                        .build())
                                .build())
                        .startDate(LocalDateTime.now().minusMonths(1))
                        .endDate(LocalDateTime.now().plusMonths(1))
                        .enabled(true)
                        .lastUpdated(LocalDateTime.now().minusDays(4))
                        .build()
        );
    }

    @Override
    public void activateCampaignForUser(List<CampaignDto> campaigns, UserProfileDto userProfile) {
        List<String> availableCampaigns = new ArrayList<>();
        for (CampaignDto campaign : campaigns) {
            MatchingResultDto matchingResult = matchingService.matchCampaignForUser(campaign, userProfile);
            if (matchingResult.isSuccess()) {
                availableCampaigns.add(campaign.getName());
            }
        }
        if (CollectionUtils.isNotEmpty(availableCampaigns)) {
            userProfileService.setActiveCampaigns(userProfile.getPlayerId(), availableCampaigns);
        }
    }
}
