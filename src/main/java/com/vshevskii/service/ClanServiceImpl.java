package com.vshevskii.service;

import com.vshevskii.dao.ClanRepository;
import com.vshevskii.dao.entities.Clan;
import com.vshevskii.dto.ClanDto;
import com.vshevskii.mappers.ProjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClanServiceImpl implements ClanService {

    private final ClanRepository clanRepository;

    private final ProjectMapper projectMapper;

    @Override
    public List<ClanDto> storeClans(List<ClanDto> devices) {
        List<Clan> clanEntities = devices.stream().map(projectMapper::toClanEntity).collect(Collectors.toList());
        List<Clan> storedClans = clanRepository.saveAll(clanEntities);
        return storedClans.stream().map(projectMapper::toClanDto).collect(Collectors.toList());
    }
}
