package com.vshevskii.service;

import com.vshevskii.dto.ClanDto;

import java.util.List;

public interface ClanService {

    List<ClanDto> storeClans(List<ClanDto> clans);
}
