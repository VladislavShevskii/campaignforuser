package com.vshevskii.service;

import com.vshevskii.dto.CampaignDto;
import com.vshevskii.dto.LevelDto;
import com.vshevskii.dto.MatchingResultDto;
import com.vshevskii.dto.UserProfileDto;
import com.vshevskii.enums.Country;
import com.vshevskii.messages.ErrorMessages;
import org.apache.commons.collections4.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public abstract class MatchingService {

    abstract MatchingResultDto matchCampaignForUser(CampaignDto campaign, UserProfileDto userProfile);

    protected boolean checkLevel(LevelDto requiredLevel, int userLevel) {
        if (requiredLevel.getMin() > requiredLevel.getMax()) {
            throw new IllegalArgumentException(ErrorMessages.INVALID_LEVEL_MIN_MAX);
        }
        return userLevel >= requiredLevel.getMin() && userLevel <= requiredLevel.getMax();
    }

    protected boolean checkCountries(List<Country> requiredCountries, Country userCountry) {
        return requiredCountries.stream().anyMatch(country -> country == userCountry);
    }

    protected boolean checkRequiredItems(List<String> requiredItems, List<String> usedItems) {
        Collection<String> missedRequiredItems = CollectionUtils.subtract(requiredItems, usedItems);
        return missedRequiredItems.isEmpty();
    }

    protected boolean checkForbiddenItems(List<String> forbiddenItems, List<String> usedItems) {
        Collection<String> usedForbiddenItems = CollectionUtils.intersection(forbiddenItems, usedItems);
        return usedForbiddenItems.isEmpty();
    }

    protected boolean checkStartEndDates(LocalDateTime campaignStartDateTime, LocalDateTime campaignEndDateTime,
                                         LocalDateTime currentDateTime) {
        LocalDate campaignStartDate = campaignStartDateTime.toLocalDate();
        LocalDate campaignEndDate = campaignEndDateTime.toLocalDate();
        LocalDate currentDate = currentDateTime.toLocalDate();
        return (currentDate.isAfter(campaignStartDate) || currentDate.isEqual(campaignStartDate)) &&
                (currentDate.isBefore(campaignEndDate) || currentDate.isEqual(campaignEndDate));
    }
}
