package com.vshevskii.service;

import com.vshevskii.dto.CampaignDto;
import com.vshevskii.dto.MatcherDto;
import com.vshevskii.dto.MatchingResultDto;
import com.vshevskii.dto.UserProfileDto;
import com.vshevskii.enums.Country;
import com.vshevskii.messages.ErrorMessages;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MatchingServiceImpl extends MatchingService {

    @Override
    public MatchingResultDto matchCampaignForUser(CampaignDto campaign, UserProfileDto userProfile) {
        String errorMessage;
        boolean success;
        MatcherDto matchers = campaign.getMatchers();
        LocalDateTime now = LocalDateTime.now();
        if (!checkLevel(matchers.getLevel(), userProfile.getLevel())) {
            errorMessage = String.format(ErrorMessages.INVALID_USER_LEVEL, userProfile.getPlayerId(),
                    userProfile.getLevel(), matchers.getLevel().getMin(), matchers.getLevel().getMax());
            success = false;
        } else if (!checkCountries(matchers.getHas().getCountries(), Country.valueOf(userProfile.getCountry()))) {
            errorMessage = String.format(ErrorMessages.INVALID_USER_COUNTRY, userProfile.getPlayerId(),
                    userProfile.getCountry(), matchers.getHas().getCountries());
            success = false;
        } else if (!checkRequiredItems(matchers.getHas().getItems(), userProfile.getInventory().getItems())) {
            errorMessage = String.format(ErrorMessages.REQUIRED_ITEM_IS_MISSING, userProfile.getPlayerId(),
                    userProfile.getInventory().getItems(), matchers.getHas().getItems());
            success = false;
        } else if (!checkForbiddenItems(matchers.getDoesNotHave().getItems(), userProfile.getInventory().getItems())) {
            errorMessage = String.format(ErrorMessages.FORBIDDEN_ITEM_IS_USED, userProfile.getPlayerId(),
                    userProfile.getInventory().getItems(), matchers.getDoesNotHave().getItems());
            success = false;
        } else if (!checkStartEndDates(campaign.getStartDate(), campaign.getEndDate(), now)) {
            errorMessage = String.format(ErrorMessages.INVALID_DATE, now.toLocalDate(),
                    campaign.getStartDate().toLocalDate(), campaign.getEndDate().toLocalDate());
            success = false;
        } else if (!campaign.isEnabled()) {
            errorMessage = String.format(ErrorMessages.DISABLED, campaign.getId(), campaign.isEnabled());
            success = false;
        }
        else {
            errorMessage = null;
            success = true;
        }
        return MatchingResultDto.builder()
                .errorMessage(errorMessage)
                .success(success)
                .build();
    }
}
