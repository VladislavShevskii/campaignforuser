package com.vshevskii.service;

import com.vshevskii.dto.CampaignDto;
import com.vshevskii.dto.UserProfileDto;

import java.util.List;

public interface CurrentCampaignService {

    List<CampaignDto> getCurrentActiveCampaigns();

    void activateCampaignForUser(List<CampaignDto> campaigns, UserProfileDto userProfile);
}
