package com.vshevskii.service;

import com.vshevskii.dto.UserProfileDto;

import java.util.List;

public interface UserProfileService {

    UserProfileDto store(UserProfileDto userProfile);

    UserProfileDto findById(String playerId);

    List<UserProfileDto> findAll();

    UserProfileDto setActiveCampaigns(String playerId, List<String> activeCampaigns);
}
