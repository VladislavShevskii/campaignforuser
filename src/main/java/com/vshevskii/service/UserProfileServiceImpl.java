package com.vshevskii.service;

import com.vshevskii.dao.UserProfileRepository;
import com.vshevskii.dao.entities.Device;
import com.vshevskii.dao.entities.UserProfile;
import com.vshevskii.dto.UserProfileDto;
import com.vshevskii.exceptions.NotFoundException;
import com.vshevskii.mappers.ProjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserProfileServiceImpl implements UserProfileService {

    private final UserProfileRepository userProfileRepository;

    private final ProjectMapper projectMapper;

    @Override
    public UserProfileDto store(UserProfileDto userProfile) {
        UserProfile entity = projectMapper.toUserEntity(userProfile);
        for (Device device : entity.getDevices()) {
            device.setUserProfile(entity);
        }
        UserProfile result = userProfileRepository.save(entity);
        return projectMapper.toUserDto(result);
    }

    @Override
    public UserProfileDto findById(String playerId) {
        Optional<UserProfile> entity = userProfileRepository.findById(UUID.fromString(playerId));
        return entity.map(projectMapper::toUserDto).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<UserProfileDto> findAll() {
        List<UserProfile> userProfiles = userProfileRepository.findAll();
        return userProfiles.stream().map(projectMapper::toUserDto).collect(Collectors.toList());
    }

    @Override
    public UserProfileDto setActiveCampaigns(String playerId, List<String> activeCampaigns) {
        Optional<UserProfile> entity = userProfileRepository.findById(UUID.fromString(playerId));
        if (entity.isPresent()) {
            entity.get().getActiveCampaigns().clear();
            entity.get().getActiveCampaigns().addAll(activeCampaigns);
            UserProfile result = userProfileRepository.save(entity.get());
            return projectMapper.toUserDto(result);
        }
        return null;
    }
}
