package com.vshevskii.migration;

import com.vshevskii.dto.ClanDto;
import com.vshevskii.dto.DeviceDto;
import com.vshevskii.dto.InventoryDto;
import com.vshevskii.dto.UserProfileDto;
import com.vshevskii.enums.Country;
import com.vshevskii.enums.Gender;
import com.vshevskii.enums.Language;
import com.vshevskii.service.ClanService;
import com.vshevskii.service.UserProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class DatabaseInitializer {

    private final ClanService clanService;

    private final UserProfileService userProfileService;

    @PostConstruct
    public void migrateData() {
        clanService.storeClans(
                Arrays.asList(
                        ClanDto.builder()
                                .name("Hello world clan")
                                .build(),
                        ClanDto.builder()
                                .name("Undead")
                                .build(),
                        ClanDto.builder()
                                .name("Human")
                                .build(),
                        ClanDto.builder()
                                .name("Night elves")
                                .build(),
                        ClanDto.builder()
                                .name("Undead")
                                .build(),
                        ClanDto.builder()
                                .name("Orcs")
                                .build()));

        userProfileService.store(UserProfileDto.builder()
                .credential("apple_credential")
                .created(LocalDateTime.parse("2021-01-10T13:37:17"))
                .modified(LocalDateTime.parse("2021-01-23T13:37:17"))
                .lastSession(LocalDateTime.parse("2021-01-23T13:37:17"))
                .totalSpent(new BigDecimal("400"))
                .totalRefund(BigDecimal.ZERO)
                .totalTransactions(5)
                .lastPurchase(LocalDateTime.parse("2021-01-22T13:37:17"))
                .activeCampaigns(Collections.emptyList())
                .devices(Arrays.asList(DeviceDto.builder()
                        .model("apple iphone 11")
                        .carrier("vodafone")
                        .firmware("123")
                        .build()))
                .level(3)
                .xp(1000)
                .totalPlaytime(144)
                .country(Country.CA.name())
                .language(Language.FRENCH.name())
                .birthdate(LocalDate.parse("2000-01-10"))
                .gender(Gender.MALE.name())
                .inventory(InventoryDto.builder()
                        .cash(new BigDecimal("123"))
                        .coins(123)
                        .items(Arrays.asList("item_1", "item_34", "item_55"))
                        .build())
                .clan(ClanDto.builder()
                        .id(1)
                        .build())
                .customField("mycustom")
                .build());

        userProfileService.store(UserProfileDto.builder()
                .credential("apple_credential")
                .created(LocalDateTime.parse("2021-02-10T13:37:17"))
                .modified(LocalDateTime.parse("2021-02-23T13:37:17"))
                .lastSession(LocalDateTime.parse("2021-02-23T13:37:17"))
                .totalSpent(new BigDecimal("400"))
                .totalRefund(BigDecimal.ZERO)
                .totalTransactions(5)
                .lastPurchase(LocalDateTime.parse("2021-02-22T13:37:17"))
                .activeCampaigns(Arrays.asList("deprecatedCampaign"))
                .devices(Arrays.asList(DeviceDto.builder()
                        .model("apple iphone 11")
                        .carrier("vodafone")
                        .firmware("123")
                        .build()))
                .level(3)
                .xp(1000)
                .totalPlaytime(144)
                .country(Country.US.name())
                .language(Language.ENGLISH.name())
                .birthdate(LocalDate.parse("2000-02-10"))
                .gender(Gender.FEMALE.name())
                .inventory(InventoryDto.builder()
                        .cash(new BigDecimal("123"))
                        .coins(123)
                        .items(Arrays.asList("item_1"))
                        .build())
                .clan(ClanDto.builder()
                        .id(1)
                        .build())
                .customField("mycustom")
                .build());
    }
}
