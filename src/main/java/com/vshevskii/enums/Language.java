package com.vshevskii.enums;

import lombok.Getter;

public enum Language {

    FRENCH("fr"),
    ENGLISH("en"),
    ROMANIAN("ro");

    Language(String name) {
        this.name = name;
    }

    @Getter
    private String name;
}
