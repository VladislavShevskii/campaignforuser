package com.vshevskii.enums;

import lombok.Getter;

public enum Gender {

    MALE("male"),
    FEMALE("female"),
    NON_BINARY("non binary");

    Gender(String name) {
        this.name = name;
    }

    @Getter
    private String name;
}
