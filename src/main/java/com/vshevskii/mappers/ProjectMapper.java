package com.vshevskii.mappers;

import com.vshevskii.dao.entities.Clan;
import com.vshevskii.dao.entities.UserProfile;
import com.vshevskii.dto.ClanDto;
import com.vshevskii.dto.UserProfileDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    UserProfile toUserEntity(UserProfileDto dto);

    @Mapping(target = "language", source = "language.name")
    @Mapping(target = "gender", source = "gender.name")
    UserProfileDto toUserDto(UserProfile entity);

    Clan toClanEntity(ClanDto dto);

    ClanDto toClanDto(Clan entity);
}
