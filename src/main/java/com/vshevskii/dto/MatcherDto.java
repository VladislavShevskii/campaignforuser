package com.vshevskii.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MatcherDto {

    private int id;

    private LevelDto level;

    private RequirementDto has;

    @JsonProperty("does_not_have")
    private ExceptRequirementDto doesNotHave;
}
