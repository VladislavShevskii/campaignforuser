package com.vshevskii.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserProfileDto {

    @JsonProperty("player_id")
    private String playerId;

    private String credential;

    private LocalDateTime created;

    private LocalDateTime modified;

    @JsonProperty("last_session")
    private LocalDateTime lastSession;

    @JsonProperty("total_spent")
    private BigDecimal totalSpent;

    @JsonProperty("total_refund")
    private BigDecimal totalRefund;

    @JsonProperty("total_transactions")
    private int totalTransactions;

    @JsonProperty("last_purchase")
    private LocalDateTime lastPurchase;

    @JsonProperty("active_campaigns")
    private List<String> activeCampaigns;

    private List<DeviceDto> devices;

    private int level;

    private int xp;

    @JsonProperty("total_playtime")
    private int totalPlaytime;

    private String country;

    private String language;

    private LocalDate birthdate;

    private String gender;

    private InventoryDto inventory;

    @JsonProperty("_customfield")
    private String customField;

    private ClanDto clan;
}
