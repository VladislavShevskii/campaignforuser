package com.vshevskii.dto;

import com.vshevskii.enums.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequirementDto {

    private int id;

    private List<Country> countries;

    private List<String> items;
}
