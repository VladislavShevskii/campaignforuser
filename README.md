# CampaignForUser

# Api List
[POST] /activate_available_campaigns/{player_id}, /activate_available_campaigns<br /><br />
[GET] /get_client_config/{player_id}, /get_client_config

# Api descripiton
/activate_available_campaigns/{player_id}   -   activate available campaigns for user profile with id 'playerId'<br /><br />
/activate_available_campaigns               -   activate available campaigns for all available user profiles<br /><br />
/get_client_config/{player_id}              -   get full user profile info by 'player_id' <br /><br />
/get_client_config                          -    get full user profile info<br /><br />

# Checking tests
There is 'MatchingServiceSpec' spock class to test checking cases

# Notes
MatchingService upgrades userProfile if all requirements are met:
- user has level between (including borders) min and max, stored in campaign.matchers.level;
- user is from on of the countries, stored in campaign.matchers.has.country;
- user has all required items, stored in campaign.matchers.has.items;
- user doesn't have any forbidden items, stored in campaign.matchers.does_not_have.items;
- current date is between (including borders) start_date and end_date, 
stored in campaign.start_date, campaign.end_date
- field 'enabled' is true, stored in campaign.enabled